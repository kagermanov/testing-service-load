package com.narick.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "[REPLICATION_Логистика_2013$Inquiry Line]")
public class InquiryLine implements Serializable {

    @Transient
    private int rowNo;

    @Id
    @Column(name = "[Inquiry No_]")
    private String inquiryNo;

    @Id
    @Column(name = "[Line No_]")
    private int lineNo;

    @Column(name = "[Customer No_]")
    private String customerNo;

    @Column(name = "[Customer Order No_]")
    private String customerOrderNo;

    @Column(name = "[BarCode]")
    private String barcode;

    @Column(name = "[Status]")
    private int status;

    @Column(name = "[Order Status]")
    private int orderStatus;

    @Transient
    private String eventDate;

    @Transient
    private String addingDate;


    public InquiryLine() {
    }

    public InquiryLine(String inquiryNo, int lineNo, String customerNo, String customerOrderNo, String barcode, int status, int orderStatus, String eventDate, String addingDate) {
        this.inquiryNo = inquiryNo;
        this.lineNo = lineNo;
        this.customerNo = customerNo;
        this.customerOrderNo = customerOrderNo;
        this.barcode = barcode;
        this.status = status;
        this.orderStatus = orderStatus;
        this.eventDate = eventDate;
        this.addingDate = addingDate;
    }

    public int getRowNo() {
        return rowNo;
    }

    public void setRowNo(int rowNo) {
        this.rowNo = rowNo;
    }

    public String getInquiryNo() {
        return inquiryNo;
    }

    public void setInquiryNo(String inquiryNo) {
        this.inquiryNo = inquiryNo;
    }

    public int getLineNo() {
        return lineNo;
    }

    public void setLineNo(int lineNo) {
        this.lineNo = lineNo;
    }

    public String getCustomerNo() {
        return customerNo;
    }

    public void setCustomerNo(String customerNo) {
        this.customerNo = customerNo;
    }

    public String getCustomerOrderNo() {
        return customerOrderNo;
    }

    public void setCustomerOrderNo(String customerOrderNo) {
        this.customerOrderNo = customerOrderNo;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(int orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public String getAddingDate() {
        return addingDate;
    }

    public void setAddingDate(String addingDate) {
        this.addingDate = addingDate;
    }

    /*
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if(!(o instanceof InquiryLine)) {
            return false;
        }
        InquiryLine bk_info = (InquiryLine) o;
        return Objects.equals(getBk_name(), bk_info.getBk_name()) && Objects.equals(getAuthor_name(), bk_info.getAuthor_name());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getBk_name(), getAuthor_name());
    }

     */

}
