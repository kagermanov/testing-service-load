package com.narick;

import com.narick.model.InquiryLine;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Random;

public class Application {

    static String topicName = "TestTopic_1";

    public static void main(String[] args) throws JMSException {
        InqLineService service = new InqLineService();

        List<InquiryLine> lineList = service.getSeveralRecords(100);


        String url = ActiveMQConnection.DEFAULT_BROKER_URL;
        //ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_BROKER_URL);
        javax.jms.ConnectionFactory connectionFactory = new ActiveMQConnectionFactory("karaf", "karaf", url);
        javax.jms.Connection connection = connectionFactory.createConnection();
        connection.start();

        // JMS messages are sent and received using a Session. We will
        // create here a non-transactional session object. If you want
        // to use transactions you should set the first parameter to 'true'
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

        Topic topic = session.createTopic(topicName);

        MessageProducer producer = session.createProducer(topic);
        producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);  // не персистное означает что возможно доставка не будет произведена

        TextMessage message = session.createTextMessage();

        int[] i = {0};
        lineList.stream().forEach((rec) -> {
                    i[0]++;
                    rec.setRowNo(i[0]);

                    /*
                    String json = new StringBuilder()
                            .append("{\"inquiryNo\": \"" + rec.getInquiryNo() + "\",")
                            .append("\"lineNo\": \"" + rec.getLineNo() + "\",")
                            .append("\"barcode\": \"" + rec.getBarcode() + "\",")
                            .append("\"status\": \"" + rec.getStatus() + "\",")
                            .append("\"customerNo\": \"" + rec.getCustomerNo() + "\",")
                            .append("\"customerOrderNo\": \"" + rec.getCustomerOrderNo() + "\",")
                            .append("\"orderStatus\": \"" + rec.getOrderStatus() + "\",")
                            .append("\"eventDate\": \"" + getCurrentDatetime() + "\",")
                            .append("\"addingDate\": \"" + getCurrentDatetime() + "\"}").toString();

                     */

                    /*
                    // sending the message
                    try {
                        message.setText(json);
                        producer.send(message);
                    } catch (JMSException e) {
                        e.printStackTrace();
                    }

                     */

                    //System.out.println(i[0] + "||" + json);
                }
        );


        Random r = new Random();
        int low = 1;
        int high = lineList.size() - 1;

        while(true) {
            lineList.stream().forEach((rec) -> {
                int index = r.nextInt(high - low) + low;
                //lineList.stream().filter(s -> s.getRowNo() == index).forEach(p -> System.out.println(p.getRowNo() + " | " + p.getBarcode()));
                lineList.stream().filter(s -> s.getRowNo() == index).forEach(p -> {
                    String json = new StringBuilder()
                            .append("{\"inquiryNo\": \"" + p.getInquiryNo() + "\",")
                            .append("\"lineNo\": \"" + p.getLineNo() + "\",")
                            .append("\"barcode\": \"" + p.getBarcode() + "\",")
                            .append("\"status\": \"" + p.getStatus() + "\",")
                            .append("\"customerNo\": \"" + p.getCustomerNo() + "\",")
                            .append("\"customerOrderNo\": \"" + p.getCustomerOrderNo() + "\",")
                            .append("\"orderStatus\": \"" + p.getOrderStatus() + "\",")
                            .append("\"eventDate\": \"" + getCurrentDatetime() + "\",")
                            .append("\"addingDate\": \"" + getCurrentDatetime() + "\"}").toString();

                            // sending the message
                            try {
                                message.setText(json);
                                producer.send(message);
                            } catch (JMSException e) {
                                e.printStackTrace();
                                try {
                                    connection.close();
                                } catch (JMSException ex) {
                                    ex.printStackTrace();
                                }
                            }
                    System.out.println("sent: " + p.getRowNo() + " | " + p.getInquiryNo() + " | " + p.getBarcode());
                });

                try {
                    Thread.sleep(5 * 10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    try {
                        connection.close();
                    } catch (JMSException ex) {
                        ex.printStackTrace();
                    }
                }

            });
        }




        //System.out.println("Sent message '" + message.getText() + "'");
        //logger.info("<<< message sent: " + message.getText() + " >>>");

        //connection.close();


    }

    private static String getCurrentDatetime() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        java.util.Date date = new java.util.Date();
        return dateFormat.format(date);
    }


}
