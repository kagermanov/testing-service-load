package com.narick;

import com.narick.model.InquiryLine;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.CriteriaQuery;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class InqLineService {
    public InquiryLine getById(Integer id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        InquiryLine inqLine = session.get(InquiryLine.class, id);
        return inqLine;
    }

    public List<InquiryLine> getSeveralRecords(Integer quant) {
        List<InquiryLine> resultSet = new ArrayList<>();

        Session session = HibernateUtil.getSessionFactory().openSession();
        Criteria criteria = session.createCriteria(InquiryLine.class);

        //criteria.add(Property.forName("barcode").eq(new String("5030518091394")));

        criteria.add(Restrictions.ge("inquiryNo", "00100017123"));
        criteria.add(Restrictions.lt("inquiryNo", "00100069413"));

        resultSet = criteria.list();

        return resultSet;
    }
}
